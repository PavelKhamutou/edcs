unmanagedJars in Compile += Attributed.blank(file("/usr/lib/jvm/java-8-jdk/jre/lib/ext/jfxrt.jar"))

To run the application you need to have sbt [http://www.scala-sbt.org/] installed with Java 8.

To run the applicatuin open 4 terminal windows and run:

```bash
$ sbt -Dakka.remote.netty.tcp.port=X run
```

where `X` is a port number (different for each instance). For example:

```bash
$ sbt -Dakka.remote.netty.tcp.port=2552 run
```


