package com.pkhamutou.drawer.ui

import scalafx.scene.paint.Color

case class Coordinates(x1: Double, x2: Double, y1: Double, y2: Double)

case class RGB(red: Double, green: Double, blue: Double, op: Double) {
  def toColor: Color = Color(red, green, blue, op)
}
