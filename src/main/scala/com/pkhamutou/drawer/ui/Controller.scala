package com.pkhamutou.drawer.ui

import javafx.collections.FXCollections

import akka.actor.{Actor, ActorLogging, ActorSystem, Props}
import com.pkhamutou.drawer.actor.CommunicationActor
import com.pkhamutou.drawer.message.Internal._
import com.pkhamutou.drawer.message.UiEvent.{HostFound, HostNotFound, ShowConnected}
import com.pkhamutou.drawer.message.{External, Internal, Message, UiEvent}

import scala.util.Try
import scalafx.Includes._
import scalafx.application.Platform
import scalafx.event.ActionEvent
import scalafx.scene.control._
import scalafx.scene.input.MouseEvent
import scalafx.scene.layout.Pane
import scalafx.scene.paint.Color
import scalafxml.core.macros.sfxml


@sfxml
class Controller(
  private val myHost: Label,
  private val myPort: Label,
  private val info: Label,
  private val host: TextField,
  private val port: TextField,
  private val connected: ListView[String],
  private val pane: Pane,
  private val colorPicker: ColorPicker,
  private val lock: ToggleButton,
  private val system: ActorSystem
) {

  setDefault()
  colorPicker.setValue(Color.Black)

  private var isLocked = false


  final class DrawerActor extends Actor with ActorLogging {

    private val ca = context.actorOf(Props[CommunicationActor], "communicator")

    override def preStart(): Unit = {
      log.info(s"$self is alive")
    }

    override def receive: Receive = {
      case HostNotFound(h, p) => Platform.runLater(info.setText(s"Cannot connect to $h:$p"))
      case HostFound => Platform.runLater(info.setText("Connected."))
      case ShowConnected(out) => Platform.runLater(connected.setItems(FXCollections.observableArrayList(out: _*)))
      case UiEvent.Disconnect => Platform.runLater(info.setText("Disconnected."))

      case Lock if sender() == ca =>
        isLocked = true
        Platform.runLater(lock.delegate.setSelected(true))

      case Unlock if sender() ==
        ca => isLocked = false
        Platform.runLater(lock.delegate.setSelected(false))

      case CleanAll if sender() == ca => Platform.runLater(pane.children.clear())

      case msg: Message => Platform.runLater(pen.drawRemote(msg.data))
      case msg: Internal => ca ! msg
      case msg: External => ca ! msg
      case msg => ca ! msg
    }
  }

  private val drawer = system.actorOf(Props(new DrawerActor()), "drawer")

  private val config = system.settings.config

  private val _host = config.getString("akka.remote.netty.tcp.hostname")
  private val _port = config.getInt("akka.remote.netty.tcp.port")

  myHost.setText(_host)
  myPort.setText(_port.toString)


  val pen = Pen(pane, drawer)


  def onMouseClicked(event: MouseEvent): Unit = if (!isLocked) pen.onClick(event, colorPicker.getValue)
  def onMouseDragged(event: MouseEvent): Unit = if (!isLocked) pen.onDrag(event, colorPicker.getValue)
  def onMouseMoved(event: MouseEvent): Unit = pen.onMove(event)


  lock.setOnAction { e =>
    if (isLocked) {
      lock.delegate.setSelected(isLocked)
    } else {
      if (lock.delegate.isSelected) {
        drawer ! Lock
      } else {
        drawer ! Unlock
      }
    }
  }

  def onClean(): Unit = {
    if (!isLocked) pane.children.clear()
  }

  def onCleanAll(): Unit = {
    if (!isLocked) {
      onClean()
      drawer ! CleanAll
    }
  }

  def onConnect(): Unit = {
    (host.getText, Try(port.getText.toInt).toOption) match {
      case (`_host`, Some(`_port`)) =>
        info.setText("Cannot connect to itself")
        setDefault()
      case (h, Some(p)) => drawer ! Connect2Node(h, p)
      case _ =>
        info.setText("Incorrect parameters")
        setDefault()
    }
  }

  def onDisconnect(): Unit = {
    drawer ! Disconnect
    info.setText("Disconnected.")
    setDefault()
  }

  def onClose(event: ActionEvent): Unit = {
    system.terminate()
    Platform.exit()
  }

  private def setDefault() = {
    port.setText("")
    host.setText("127.0.0.1")
  }
}


