package com.pkhamutou.drawer.ui

import akka.actor.ActorRef
import com.pkhamutou.drawer.message.Internal.Data

import scalafx.scene.input.MouseEvent
import scalafx.scene.layout.Pane
import scalafx.scene.paint.Color
import scalafx.scene.shape.Line

case class Pen(pane: Pane, drawer: ActorRef) {
  private val width = 5

  def draw(cs: Coordinates, color: Color): Unit = {
    import cs._
    val line = Line(x1, y1, x2, y2)
    line.setStrokeWidth(width)
    line.setStroke(color)
    drawer ! Data(cs, RGB(color.red, color.green, color.blue, color.opacity))
    pane.children.add(line)
  }

  def drawRemote(data: Data): Unit = {
    import data.cs._
    val line = Line(x1, y1, x2, y2)
    line.setStrokeWidth(width)
    line.setStroke(data.rgb.toColor)
    pane.children.add(line)
  }

  private var ox = 0d
  private var oy = 0d
  private var lastTime = System.currentTimeMillis()

  def onClick(event: MouseEvent, color: Color): Unit = {
    val x = event.getX
    val y = event.getY
    draw(Coordinates(x, x, y, y), color)
    ox = x
    oy = y
  }

  def onDrag(event: MouseEvent, color: Color): Unit = {
    if (System.currentTimeMillis - lastTime < 40) ()
    else {
      val x = event.getX
      val y = event.getY
      draw(Coordinates(ox, x, oy, y), color)
      ox = x
      oy = y
      lastTime = System.currentTimeMillis()
    }
  }

  def onMove(event: MouseEvent): Unit = {
    ox = event.getX
    oy = event.getY
  }
}
