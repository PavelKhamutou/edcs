package com.pkhamutou.drawer


import akka.actor.ActorSystem

import scala.reflect.runtime.universe.typeOf
import scalafx.Includes._
import scalafx.application.JFXApp
import scalafx.scene.Scene
import scalafxml.core.{DependenciesByType, FXMLView}

object Main extends JFXApp {

  val table = getClass.getResource("/table.fxml")

  val root = FXMLView(table, new DependenciesByType(
    Map(typeOf[ActorSystem] -> ActorSystem("drawer"))
  ))

  stage = new JFXApp.PrimaryStage() {
    title = "Akka Drawer"
    scene = new Scene(root)
  }

}
