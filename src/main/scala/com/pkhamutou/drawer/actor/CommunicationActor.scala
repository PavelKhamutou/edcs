package com.pkhamutou.drawer.actor

import akka.actor._
import akka.util.Timeout
import com.pkhamutou.drawer.message.External._
import com.pkhamutou.drawer.message.Internal._
import com.pkhamutou.drawer.message.{External, Internal, Message, UiEvent}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.duration.DurationInt
import scala.util.{Failure, Success}


class CommunicationActor extends Actor with ActorLogging {

  case object SendConnected

  import CommunicationActor._

  type Out = List[ActorRef]

  private val id = scala.util.Random.nextLong()
  private implicit val timeout = Timeout(10.seconds)

  private val name = self.path.name
  private val parent = context.parent.path.name

  context.system.scheduler.schedule(1.second, 1.second, self, SendConnected)

  override def receive: Receive = process(None, Nil)(None)

  def process(in: Option[ActorRef], out: Out)(implicit locker: Option[ActorRef]): Receive = {

    case SendConnected => context.parent ! UiEvent.ShowConnected(out.map(_.toString))

    case LockTable(`self`) =>
    case UnlockTable(`self`) =>
    case External.CleanAll(`id`) =>

    case msg: Internal => internalFlow(in, out, msg)
    case msg: External => externalFlow(in, out, msg)

    case Terminated(ref) if in.contains(ref) =>
      context.parent ! UiEvent.Disconnect
      log.info(s"$ref is no longer alive")
      val l = if (locker.contains(ref)) {
        context.parent ! Unlock
        broadcast(UnlockTable(ref), out.filter(_ != ref))
        None
      } else locker
      context.become(process(None, out.filter(_ != ref))(l))

    case Terminated(ref) =>

      val l = if (locker.contains(ref)) {
        context.parent ! Unlock
        broadcast(UnlockTable(ref), out.filter(_ != ref))
        None
      } else locker

      log.info(s"$ref is no longer alive")
      context.become(process(in, out.filter(_ != ref))(l))

    case Message(_, `id`) => // do nothing

    case msg: Message =>
      log.info(s"Get data from ${sender()}")
      context.parent ! msg
      broadcast(msg, out)

    case msg => log.error(s"Received unknown message: $msg")
  }


  def internalFlow(in: Option[ActorRef], out: Out, msg: Internal)(implicit locker: Option[ActorRef]): Unit = msg match {

    case Internal.CleanAll => broadcast(External.CleanAll(id), out)
    case msg: Data if locker.isEmpty => broadcast(Message(msg, id), out)

    case Connect2Node(host, port) if in.isEmpty => connectTo(host, port, out)
    case Disconnect if in.isDefined => in.foreach { ref =>
        ref ! DisconnectMe(self)
        context.unwatch(ref)
        context.become(process(None, out.remove(ref)))
      }
    case Lock => broadcast(LockTable(self), out)

    case Unlock => broadcast(UnlockTable(self), out)
    case msg => log.error(s"Received unknown  Internal message: $msg")
  }

  def externalFlow(in: Option[ActorRef], out: Out, msg: External)(implicit locker: Option[ActorRef]): Unit = msg match {

    case Request2Connect(ref) =>
      context.watch(ref)
      context.become(process(in, ref :: out))

    case DisconnectMe(ref) =>
      context.unwatch(ref)
      context.become(process(in, out.remove(ref)))

    case msg @ External.CleanAll(_) =>
      context.parent ! Internal.CleanAll
      broadcast(msg, out)

    case l @ LockTable(by) =>
      context.parent ! Lock
      broadcast(l, out)
      context.become(process(in, out)(Some(by)))

    case ul @ UnlockTable(_) =>
      context.parent ! Unlock
      broadcast(ul, out)
      context.become(process(in, out)(None))

    case msg => log.error(s"Received unknown External message: $msg")
  }

  def broadcast(msg: Any, out: Out): Unit =
    out.toSet[ActorRef].filter(_ != sender()).foreach(_ ! msg)

  def connectTo(host: String, port: Int, out: Out)(implicit locker: Option[ActorRef]): Unit = {
    context.actorSelection(s"akka.tcp://drawer@$host:$port/user/$parent/$name")
      .resolveOne()
      .onComplete {
        case Success(ref) =>
          context.watch(ref) // self will receive Terminated if ref dies
          context.become(process(Some(ref), ref :: out))
          ref ! Request2Connect(self) // request ref to add self as a peer
          context.parent ! UiEvent.HostFound
        case Failure(ActorNotFound(_)) =>
          context.parent ! UiEvent.HostNotFound(host, port)
          log.info(s"Node $host:$port is not found")
        case Failure(failure) => failure.printStackTrace()
      }
  }
}

object CommunicationActor {
  implicit final class listRich(self: List[ActorRef]) {
    def remove(ref: ActorRef): List[ActorRef] = (self.toBuffer -= ref).toList
  }
}
