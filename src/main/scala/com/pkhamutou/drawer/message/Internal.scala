package com.pkhamutou.drawer.message

import com.pkhamutou.drawer.ui.{Coordinates, RGB}

sealed trait Internal

object Internal {
  case class Connect2Node(host: String, port: Int) extends Internal
  case object Disconnect extends Internal
  case object Lock extends Internal
  case object Unlock extends Internal
  case class Data(cs: Coordinates, rgb: RGB) extends Internal
  case object CleanAll extends Internal
}
