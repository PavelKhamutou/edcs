package com.pkhamutou.drawer.message

import akka.actor.ActorRef

sealed trait External

object External {
  case class Request2Connect(ref: ActorRef) extends External
  case class DisconnectMe(ref: ActorRef) extends External
  case class LockTable(ref: ActorRef) extends External
  case class UnlockTable(ref: ActorRef) extends External
  case class CleanAll(id: Long) extends External

}
