package com.pkhamutou.drawer.message

import com.pkhamutou.drawer.message.Internal.Data

case class Message(data: Data, id: Long)
