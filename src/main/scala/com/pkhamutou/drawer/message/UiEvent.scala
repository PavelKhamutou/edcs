package com.pkhamutou.drawer.message

trait UiEvent

object UiEvent {
  case object HostFound extends UiEvent
  case class HostNotFound(host: String, port: Int) extends UiEvent
  case class ShowConnected(out: List[String]) extends UiEvent
  case object Disconnect extends UiEvent

}
