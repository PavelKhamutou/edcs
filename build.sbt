name := "akka-drawer"

version := "1.0"

scalaVersion := "2.12.2"

lazy val akkaVersion = "2.5.2"

addCompilerPlugin("org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full)

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-remote" % akkaVersion,
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion,
  "org.scalafx" % "scalafx_2.12" % "8.0.102-R11",
  "org.scalafx" %% "scalafxml-core-sfx8" % "0.3",
  "org.scalatest" %% "scalatest" % "3.0.1" % "test"
)

unmanagedJars in Compile += Attributed.blank(file("/usr/lib/jvm/java-8-jdk/jre/lib/ext/jfxrt.jar"))
